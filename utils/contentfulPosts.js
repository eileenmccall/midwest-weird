var space = process.env.CONTENTFUL_SPACE_ID;
var accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;

var client = require('contentful').createClient({
    space: space,
    accessToken: accessToken,
});

async function fetchCaseFileIds () {
    var entries = await client.getEntries({ content_type: "caseFile" });
    return entries.items.map(entry => entry.sys.id);
}

async function fetchCaseFiles() {
    var entries = await client.getEntries({ content_type: "caseFile" });

    return entries.items 
        ? entries.items.map(mapCaseFile)
        : console.error(`Error getting Entries for ${contentType.name}.`);
}

async function fetchCaseFileById (id) {
    var entry = await client.getEntry(id);

    return entry
        ? mapCaseFile(entry)
        : null;
}

async function fetchResources () {
    var resources = await client.getEntries({ content_type: "resource" });

    if (!resources.items) return;

    return resources.items.reduce(function (acc, cur) {
        var key = cur.fields.type;
        var resource = {
            ...cur.fields,
            id: cur.sys.id
        };

        return {
            ...acc,
            [key]: [
                ...acc[key] || [],
                resource
            ]
        };
        // return {};
    }, {});
}

export default { fetchCaseFiles, fetchCaseFileById, fetchCaseFileIds, fetchResources };

function mapCaseFile (
    { 
        fields, 
        sys: { 
            id
        } 
    }
) {
    return { ...fields, id }
}