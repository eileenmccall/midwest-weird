import styled from '@emotion/styled'
import Head from 'next/head'
import { Alert, Col, Row } from 'react-bootstrap';
import Layout from '../components/Layout';
import LinkCard from '../components/LinkCard';
import SiteDescription from '../components/SiteDescription';
import SiteTitle from '../components/SiteTitle';
import styles from '../styles/Home.module.css'

var StyledP = styled.p`
    color: blue;
`;

export default function Home () {

    return (
        <Layout>
            <Row className="justify-content-center">
                <Col lg={8} xs={12}>
                    <Alert variant="warning">
                        "The site is still under construction, some things may not work yet." 
                        <br/> 
                        - Management
                    </Alert>
                    <div className={styles.container}>
                        <main className={styles.main}>
                            <StyledDiv>
                                <div>
                                    <LinkCard 
                                        title="Case Files"
                                        link="/case-files"
                                        description="Explore the archives. 
                                            Ghosts, UFOs, Cryptids and other 
                                            weirdness from around the midwest."
                                    />
                                </div>
                                <div>
                                    <LinkCard 
                                        title="Make a report"
                                        link="/contact"
                                        description="Have you had a paranormal 
                                            experience? A brush with the unknown? 
                                            We want to know about it!"
                                    />
                                </div>
                                <div>
                                    <LinkCard 
                                        title="Resources"
                                        link="/resources"
                                        description="Reading lists, websites, 
                                            documentaries, and awesome paranormal 
                                            people to follow."
                                    />
                                </div>
                                <div>
                                    <LinkCard 
                                        title="Support us"
                                        link="https://patreon.com/eileenmccall"
                                        description="Weird Midwest is a side-project 
                                        for now. Donate today to help us put more time 
                                        and effort into it."
                                    />
                                </div>
                            </StyledDiv>
                        </main>
                    </div>
                </Col>
            </Row>
        </Layout>
    )
}

var StyledDiv = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: stretch;

    div {
        flex-basis: calc(50% - 16px);
        margin: 8px;
    }

    @media(max-width: 575px) {
        div {
            flex-basis: 100%;
            margin: 8px 0;
        }
    }
`;