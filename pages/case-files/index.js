import Head from 'next/head'
import React from 'react'
import { Col, Container, Row, Table } from 'react-bootstrap'
import BackButton from '../../components/BackButton'
import Layout from '../../components/Layout'
import Papers from '../../components/Papers'
import dataQueries from '../../utils/contentfulPosts'

export default function CaseFile ({ caseFiles = []}) {

    return (
        <Layout>
            <Row className="justify-content-md-center">
                <Col md={10}>
                    <Papers>
                        <BackButton />
                        <h2>Case files</h2>

                        <Table borderless striped>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    caseFiles.map(function ({ title, city, state, type, id }) {
                                        return (
                                            <tr key={ id } >
                                                <td><a href={`/case-files/${id}`}>{ title }</a></td>
                                                <td>{ type }</td>
                                                <td>{ city }, { state }</td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </Table>
                    </Papers>
                </Col>
            </Row>
        </Layout>
    );
}

export async function getStaticProps() {
    const caseFiles = await dataQueries.fetchCaseFiles();

    return {
        props: {
            caseFiles,
        },
    };
}