import React, { useRef } from 'react'
import { Col, Row } from 'react-bootstrap';
import BackButton from '../../components/BackButton';
import Layout from '../../components/Layout';
import Papers from '../../components/Papers';
import dataQueries from '../../utils/contentfulPosts';
import remark, { parse } from "remark";
import html from "remark-html";

export default function CaseFile ({ caseFile }) {

    var { current: bodyHTML } = useRef(
        remark()
            .use(parse)
            .use(html)
            .processSync(caseFile.body)
    );

    return (
        <Layout>
            <Row className="justify-content-center">
                <Col md={10}>
                    <Papers>
                        <BackButton path="/case-files" />
                        <h1>{ caseFile.title }</h1>
                        <div dangerouslySetInnerHTML={{ __html: bodyHTML}}></div>
                        {
                            caseFile.sources
                                ? (
                                    <>
                                        <hr />
                                        <h3>Sources:</h3>
                                        <ul>
                                            {
                                                caseFile.sources.map(function (source) {
                                                    return (<li key={source}>{ source }</li>);
                                                })
                                            }
                                        </ul>
                                    </>
                                )
                                : (null)
                        }
                    </Papers>
                </Col>
            </Row>
        </Layout>
    )
}

export async function getStaticPaths () {
    var ids = await dataQueries.fetchCaseFileIds();

    var paths = ids.map(id => ({ params: { id } }));

    return { paths, fallback: false };
}

export async function getStaticProps ({ params: { id }}) {
    var caseFile = await dataQueries.fetchCaseFileById(id);

    return {
        props: {
            caseFile
        }
    };
}