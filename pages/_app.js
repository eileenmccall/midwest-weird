import '../styles/globals.css';
import "../styles/overrides.scss";


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
