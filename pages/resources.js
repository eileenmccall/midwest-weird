import Link from 'next/link'
import React from 'react'
import BackButton from '../components/BackButton'
import Layout from '../components/Layout'
import Papers from '../components/Papers'
import dataQueries from "../utils/contentfulPosts";

export default function Resources({ resources }) {

    return (
        <Layout>
            <Papers>
                <BackButton />
                <h1>Resources:</h1>
                <h3>Books</h3>
                <ul>
                    { mapResources(resources.book) }
                </ul>

                <h3>Documentaries</h3>
                <ul>
                    { mapResources(resources.documentary) }
                </ul>

                <h3>Paranormal People</h3>
                <ul>
                    { mapResources(resources.person) } 
                </ul>

                (Have a cool project or resource to share? <Link href="/contact">Contact us!</Link>)
            </Papers>

        </Layout>
    );

    function mapResources (resources) {
        return resources.map(function mapToListItem (book) {
            return (
                <li key={book.id}><a href={book.link}>{book.text}</a></li>
            )
        })
    }
}

export async function getStaticProps() {
    const resources = await dataQueries.fetchResources();

    return {
        props: {
            resources,
        },
    };
}