import Head from 'next/head';
import React from 'react';
import SiteTitle from "./SiteTitle";
import SiteDescription from "./SiteDescription";
import { Container } from 'react-bootstrap';
import Footer from './Footer';
import Header from './Header';

export default function Layout({ children }) {
    return (
        <>
            <Head>
                <title>Weird Midwest</title>
                <link rel="icon" href="/favicon.ico" />
                <link href="https://fonts.googleapis.com/css2?family=Courier+Prime:ital,wght@0,400;0,700;1,400&family=Nova+Mono&display=swap" rel="stylesheet"></link>
            </Head>

            <Header />

            <main style={{ paddingBottom: "150px" }}>
                <Container>
                    { children }
                </Container>

                <Footer />
            </main>

        </>
    )
}
