import react from "react";
import styled from '@emotion/styled';
import { css } from "@emotion/core";
import PostHeader from "./PostHeader";

export default function Papers ({ children }) {
    return (
        <StyledDiv>
            { children }
        </StyledDiv>
    );
}

var shadowStyles = css`
    box-shadow: 1px 1px 1px rgba(0,0,0,0.25);
    border: 1px solid #bbb;
`;

var StyledDiv = styled.div`
    background: #fff;
    padding: 50px;
    position: relative;
    ${shadowStyles}
    
    &::before,
    &::after {
        ${shadowStyles};
        content: "";
        position: absolute;
        height: 95%;
        width: 99%;
        background-color: #eee;
    }

    &::before {
        right: 15px;
        top: 0;
        transform: rotate(-1deg);
        z-index: -1;
    }

    &::after {
        top: 5px;
        right: -5px;
        transform: rotate(1deg);
        z-index: -2;
    }
    /* border: 1px solid lightgray;
    background: #fff;
    box-shadow:
        /* The top layer shadow */
        /* 0 -1px 1px rgba(0,0,0,0.15), */
        /* The second layer */
        /* 0 -10px 0 -5px #eee, */
        /* The second layer shadow */
        /* 0 -10px 1px -4px rgba(0,0,0,0.15), */
        /* The third layer */
        /* 0 -20px 0 -10px #eee, */
        /* The third layer shadow */
        /* 0 -20px 1px -9px rgba(0,0,0,0.15); */
        /* Padding for demo purposes */
    /* padding: 30px; */
`;

