import React from 'react'
import Link from "next/link";

export default function BackButton ({ path = "/" }) {
    return (
        <Link href={ path }>&larr; Back</Link>
    );
}
