import styled from '@emotion/styled';
import React from 'react'

export default function LinkCard ({ title, link, description }) {
    return (
        <StyledA href={link}>
            <h3>{ title } &rarr;</h3>
            <p>{ description }</p>
        </StyledA>
    )
}

var StyledA = styled.a`
    background-color: white;
    border: 1px solid black;
    border-radius: 10px;
    color: inherit;
    display: block;
    height: 100%;
    padding: 1.5rem;
    text-align: left;
    text-decoration: none;
    transition: color 0.15s ease, border-color 0.15s ease;

    &:hover,
    &:focus,
    &:active {
        color: #0070f3;
        border-color: #0070f3;
    }

    h3 {
        margin: 0 0 1rem 0;
        font-size: 1.5rem;
    }

    p {
        margin: 0;
        font-size: 1.25rem;
        line-height: 1.5;
    }
`;

