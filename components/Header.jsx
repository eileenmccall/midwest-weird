import styled from '@emotion/styled'
import React from 'react'
import SiteDescription from './SiteDescription'
import SiteTitle from './SiteTitle'

export default function Header () {
    return (
        <StyledHeader>
            <SiteTitle />
            <SiteDescription />
        </StyledHeader>
    )
}

var StyledHeader = styled.header`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 50px 25px;
`;
