import styled from '@emotion/styled'
import React from 'react'

export default function SiteDescription() {
    return (
        <StyledP>
            An archive of midwestern weirdness,
            paranormal activity, & high strangeness.
        </StyledP>
    )
}

var StyledP = styled.p`
    font-size: 1.5rem;
    line-height: 1.5;
    margin-bottom: 0;
    max-width: 600px;
    text-align: center;
`;
