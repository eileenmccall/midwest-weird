import styled from '@emotion/styled'
import React from 'react'

export default function SiteTitle() {
    return (
        <StyledDiv>
            <StyledImg src="/alien_icon.svg" alt="Alien Icon" className="" />
            <StyledH1><StyledA href="/">Weird Midwest</StyledA></StyledH1>
            <StyledImg src="/bigfoot_icon.svg" alt="Bigfoot Icon" />
        </StyledDiv>
    )
}

var StyledA = styled.a`
    text-decoration: none;
    color: black;

    &:hover {
        color: black;
        text-decoration: none;
    }
`;

var StyledDiv = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

var StyledImg = styled.img`
    max-height: 35px;

    :first-of-type {
        height: 30px;
    }
`;

var StyledH1 = styled.h1`
    font-size: 4rem;
    line-height: 1.15;
    margin: 0 25px;
    text-align: center;
`;
