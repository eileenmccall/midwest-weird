import styled from '@emotion/styled'
import React from 'react'

export default function PostHeader({ children }) {
    return (
        <StyledH2>
            { children }
        </StyledH2>
    )
}

var StyledH2 = styled.h2`
    font-family: 'Courier Prime';
`;