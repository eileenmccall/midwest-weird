import styled from '@emotion/styled'
import React from 'react'

export default function Footer () {
    return (
        <StyledFooter>
            Built by{' '}
            <a href="http://twitter.com/queenbigdick">
                Eileen McCall
            </a>
        </StyledFooter>
    )
}

var StyledFooter = styled.footer`
    align-items: center;
    background-color: rgba(255,255,255,66%);
    border-top: 1px solid #eaeaea;
    bottom: 0;
    display: flex;
    height: 100px;
    justify-content: center;
    position: absolute;
    width: 100%;

    a {
        align-items: center;
        display: flex;
        justify-content: center;
        margin-left: 12px;
    }
`;